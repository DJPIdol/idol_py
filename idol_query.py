#!/usr/bin/env python
import idol.aci as aci
import urllib
import string
import re

def query(min_date='',max_date=''):
  # idol server parameters
  # query_action = "GetQueryTagValues&Text=DREALL&DatabaseMatch=metrics_test&"+\
    #"MinDate="+min_date+"&MaxDate="+max_date+"&DocumentCount=true&FieldName="+\
    #"TEST_NEGATIVE%2CTEST_GENERAL%2CTEST_POSITIVE&Sort=DocumentCount"
  query_action = "Query&Text=*&DatabaseMatch=metrics_test&MinDate="+\
    min_date+"&MaxDate="+max_date+"&DocumentCount=true&print=none&"+\
    "printfields=TEST_NEGATIVE%2CTEST_GENERAL%2CTEST_POSITIVE%2CDREDATE%2CDRETITLE"
  # iterate over <autn:hit>
  keys = ['DRETITLE','DREDATE','TEST_NEGATIVE','TEST_POSITIVE','TEST_GENERAL']
  autnresponse = aci.action(query_action)
  response = autnresponse.find("./response").text
  if response == "SUCCESS":
    numhits = int(autnresponse.find(
      "./responsedata/{http://schemas.autonomy.com/aci/}numhits").text)
    if numhits > 0:
      header = ""
      for col in keys:
        header = header+col+","
      print header
      for hit in autnresponse.findall(
        "./responsedata/{http://schemas.autonomy.com/aci/}hit"):
        row = ""
        doc = hit.find("{http://schemas.autonomy.com/aci/}content/DOCUMENT")
        for col in keys:
          cell = doc.find(col)
          if cell != None:
            row = row+cell.text
          row = row+","
        print row
