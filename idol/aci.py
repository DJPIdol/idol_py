#!/usr/bin/env python
import httplib
import urllib
import config
try:
  # python 2.4.3 compatibility
  from elementtree import ElementTree
except ImportError:
  import xml.etree.cElementTree as ElementTree

def cat_action(action_param_value_pairs):
  #print (action_param_value_pairs)
  # build connection to IDOL
  conn = httplib.HTTPConnection(config.server+":"+str(config.cat_port))
  conn.request("GET", "/action="+action_param_value_pairs)
  r1 = conn.getresponse()
  # read the xml as a string
  s = r1.read()
  conn.close()

  # return raw text
  return ElementTree.fromstring(s)

def action(action_param_value_pairs):
  #print (action_param_value_pairs)
  # build connection to IDOL
  conn = httplib.HTTPConnection(config.server+":"+str(config.aci_port))
  conn.request("GET", "/action="+action_param_value_pairs)
  r1 = conn.getresponse()
  # read the xml as a string
  s = r1.read()
  conn.close()

  # return raw text
  return ElementTree.fromstring(s)

