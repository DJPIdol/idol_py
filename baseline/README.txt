Hi Daniel, 

this archive should have eveythign to replicate.

the input data was a CSV whiich is now indexed to IDOL after lots of ETL in the cfs lua stage

what I have now is :
an IDX with the exported documents
a python script [cat_train.py] to index that IDX to content & 
0) create 4 classifiers
1) extract all classes for each classifier using gettagvalues
2) create a state for each class with a random sample using fieldtext/parametric
3) train each class using that state
(-) I had code to remove documents consumed by states post training all classes
but I too that out as there was not enough data left
+ and other python script to test
4) extract all documents, classify vs each classifier and compare the tag then generate metrics

What I am seeing right now is:
classifiers = [ "ANA_BA_L_K", "ANKET_AD_", "K_K_NEDEN", "SONU_"]
# if I use the three numerics + 1 text I get around 33%
#feature_fields = "SORU_1,SORU_2,SORU_3,SORU_4"
# if I use the three numerics NO text I get around 44%
feature_fields = "SORU_1,SORU_2,SORU_3"

also, they had 25% rubbish fields tagged as QA. I removed those and... the results got 3% worse in 4 field mode & 1% worse in 1 field mode, which makes me think the source data is probabaly inconsistent :-(



