#!/usr/bin/env python
# -*- coding: utf-8 -*-
import idol.aci as aci
import idol.index as index
import urllib
import string
import re
#import trans
import time

####
#classifiers = [ 'ANA_BA_L_K', 'ANKET_AD_', 'K_K_NEDEN', 'SONU_']
#classifiers = [ 'K_K_NEDEN']
#classifiers = [ 'ANA_BA_L_K']
classifiers = [ 'SONU_','ANKET_AD_']
#classifiers = [ 'ANKET_AD_']
ff_scenarios = ['SORU_1,SORU_2,SORU_3']
_ff_scenarios = ['SORU_1','SORU_2','SORU_3','SORU_4',
'SORU_1,SORU_2',
'SORU_1,SORU_3',
'SORU_1,SORU_4',
'SORU_2,SORU_3',
'SORU_2,SORU_4',
'SORU_3,SORU_4',
'SORU_1,SORU_2,SORU_3',
'SORU_1,SORU_2,SORU_4',
'SORU_1,SORU_3,SORU_4',
'SORU_2,SORU_3,SORU_4',
'SORU_2,SORU_3,SORU_4',
'SORU_1,SORU_2,SORU_3,SORU_4']
feature_fields = [ 'SORU_1', 'SORU_2', 'SORU_3', 'SORU_4' ]
outFile = open('cat_train_test.csv','w')
sample_size = 250
sort = "random"
states = []
idx = "dk.idx-0.idx"
sleep_time = 5

def safe_enc(s):
  return s.encode('utf-8')

def init_results(classifier):
  action = 'getquerytagvalues&text=DREALL&fieldname='
  results[classifier] = dict()
  summary[classifier] = \
        {'true': {'positive': 0, 'negative': 0}, 
        'false': {'positive': 0, 'negative': 0}}
  autnresponse = aci.action(action+classifier)
  field = autnresponse.find(
    './responsedata/{http://schemas.autonomy.com/aci/}field')
  values = int(field.find(
    '{http://schemas.autonomy.com/aci/}number_of_values').text)
  if values > 0:
    for value in field.findall('{http://schemas.autonomy.com/aci/}value'):
      results[classifier][value.text] = \
        {'true': {'positive': 0, 'negative': 0}, 
        'false': {'positive': 0, 'negative': 0}}

def doc_class_class(classifier,drereference):
  cat_action = 'ClassifierQuery&ClassifierName='+safe_enc(classifier)+\
    '&DocRef='+drereference
  autnresponse = aci.cat_action(cat_action)
  #print autnresponse
  return autnresponse.find(
    './responsedata/{http://schemas.autonomy.com/aci/}class').text

def doc_classify(document,classifier):
  drereference = document.find('DREREFERENCE').text
  #print (drereference)
  #for classifier in classifiers:
  #print (classifier)
  expected_tag = document.find(classifier)
  if expected_tag is not None:
    expected_value = expected_tag.text.upper()
    measured_value = doc_class_class(classifier,drereference)
    #print ('expected '+expected_value+' vs measured '+ measured_value)
    if expected_value == measured_value:
      for value in results[classifier]:
        if value == expected_value:
          results[classifier][value]['true']['positive'] += 1
        else:
          results[classifier][value]['true']['negative'] += 1
    else:
      for value in results[classifier]:
        if value == expected_value:
          results[classifier][value]['false']['negative'] += 1
        elif value == measured_value:
          results[classifier][value]['false']['positive'] += 1
        else:
          results[classifier][value]['true']['negative'] += 1

def traverse_docs(classifier):
  action = 'list&maxresults=10000'
  autnresponse = aci.action(action)
  responsedata = autnresponse.find('./responsedata')
  for hit in responsedata.findall(
    '{http://schemas.autonomy.com/aci/}hit'):
    document = hit.find('{http://schemas.autonomy.com/aci/}content/DOCUMENT')
    doc_classify(document,classifier)
 
def present_results(classifier,ff):
  outFile.write ('summary by classifier/class\n')
  outFile.write ('classifier,fields,class,true positive,false positive,true negative,'+\
    'false negative\n')
  for class_class in results[classifier]:
    outFile.write ((classifier+','+ff+','+class_class+','+\
      str(results[classifier][class_class]['true']['positive'])+','+\
      str(results[classifier][class_class]['false']['positive'])+','+\
      str(results[classifier][class_class]['true']['negative'])+','+\
      str(results[classifier][class_class]['false']['negative'])+'\n').\
      encode('utf-8'))
    summary[classifier]['true']['positive'] += \
      results[classifier][class_class]['true']['positive']
    summary[classifier]['true']['negative'] += \
      results[classifier][class_class]['true']['negative']
    summary[classifier]['false']['positive'] += \
      results[classifier][class_class]['false']['positive']
    summary[classifier]['false']['negative'] += \
      results[classifier][class_class]['false']['negative']
  outFile.write ('summary by classifier'+'\n')
  outFile.write ('classifier,field,,true positive,false positive,true negative,'+\
    'false negative'+'\n')
  outFile.write (classifier+','+ff+',,'+\
    str(summary[classifier]['true']['positive'])+','+\
    str(summary[classifier]['false']['positive'])+','+\
    str(summary[classifier]['true']['negative'])+','+\
    str(summary[classifier]['false']['negative'])+'\n')
  overview['true']['positive'] += \
    summary[classifier]['true']['positive']
  overview['true']['negative'] += \
    summary[classifier]['true']['negative']
  overview['false']['positive'] += \
    summary[classifier]['false']['positive']
  overview['false']['negative'] += \
    summary[classifier]['false']['negative']
  outFile.write ('summary overview'+'\n')
  outFile.write (',field,,true positive,false positive,true negative,false negative'+'\n')
  outFile.write (','+ff+',,'+\
    str(overview['true']['positive'])+','+\
    str(overview['false']['positive'])+','+\
    str(overview['true']['negative'])+','+\
    str(overview['false']['negative'])+'\n')
  outFile.write (classifier+',"'+ff+'",Precision,'+str(overview['true']['positive']/ \
    float(overview['true']['positive']+overview['false']['positive']))+'\n')
  outFile.write (classifier+',"'+ff+'",Recall,'+str(overview['true']['positive']/ \
    float(overview['true']['positive']+overview['false']['negative']))+'\n')


###
def load_content(idx):
  action = "DREINITIAL?"
  autnresponse = index.action(action)
  print(autnresponse)
  time.sleep(sleep_time)
  action = "DREADD?"+idx
  autnresponse = index.action(action)
  print(autnresponse)
  time.sleep(sleep_time)
  action = "DRESYNC?"
  autnresponse = index.action(action)
  print(autnresponse)
  time.sleep(sleep_time)

def safe_enc(s):
  return s.encode('utf-8')

def classifier_create(classifier,ff):
  # delete it in case it exists
  cat_action = "ClassifierDelete&ClassifierName="+classifier
  autnresponse = aci.cat_action(cat_action)
  # now create it
  cat_action = "ClassifierCreate&ClassifierType=RandomForest&"+\
    "ClassifierName="+classifier+"&FeatureFields="+ff
  autnresponse = aci.cat_action(cat_action)
  print autnresponse
  response = autnresponse.find("./response").text
  return response

def get_classes(classifier):
  action = "getQueryTagValues&fieldName="+classifier+"&text=DREALL"
  autnresponse = aci.action(action)
  response = autnresponse.find("./response").text
  classes = []
  if response == "SUCCESS":
    field = autnresponse.find(
      "./responsedata/{http://schemas.autonomy.com/aci/}field")
    values = int(field.find(
      "{http://schemas.autonomy.com/aci/}number_of_values").text)
    if values > 0:
      for value in field.findall("{http://schemas.autonomy.com/aci/}value"):
        classes.append(value.text)
  return classes

def create_class(classifier,cl_class):
  cat_action = "ClassifierAddClass&ClassifierName="+classifier+\
    "&ClassName="+safe_enc(cl_class)
  autnresponse = aci.cat_action(cat_action)
  response = autnresponse.find("./response").text
  return response

def training_state(classifier,cl_class,ff):
  action = "query&fieldText=MATCH{"+safe_enc(cl_class)+"}:"+classifier+\
    "&maxresults="+str(sample_size)+"&print=none&printfields="+ff+\
    "&text=DREALL&storestate=true&sort="+sort
  autnresponse = aci.action(action)
  response = autnresponse.find("./response").text
  state = ""
  if response == "SUCCESS":
    numhits = autnresponse.find(
      "./responsedata/{http://schemas.autonomy.com/aci/}numhits").text
    state = autnresponse.find(
      "./responsedata/{http://schemas.autonomy.com/aci/}state").text
    print "created training state ["+state+"] for classifier ["+\
      classifier+"] class ["+cl_class+"] with ["+numhits+"] documents"
  states.append(state)
  return state

def set_class_training(classifier,cl_class,ff):
  cat_action = "ClassifierSetClassTraining&ClassifierName="+classifier+\
    "&ClassName="+safe_enc(cl_class)+"&StateID="+training_state(classifier,cl_class,ff)
  autnresponse = aci.cat_action(cat_action)
  response = autnresponse.find("./response").text
  return response

def create_classes(classifier,ff):
  for cl_class in get_classes(classifier):
    print "Creating Classifier ["+classifier+"] fields ["+ff+"] class ["+cl_class+\
      "] response ["+create_class(classifier,cl_class)+"]"
    print "Adding Class Training to Classifier ["+classifier+"] class ["+\
      cl_class+"] response ["+set_class_training(classifier,cl_class,ff)+"]"

def train_classifier(classifier):
  cat_action = "ClassifierTrain&ClassifierName="+classifier
  autnresponse = aci.cat_action(cat_action)
  response = autnresponse.find("./response").text
  return response

#def create_classifiers(classifiers):
  #for classifier in classifiers:
    #print "creating classifier ["+classifier+"] response ["+\
      #classifier_create(classifier)+"]"
    #create_classes(classifier)
    #print "Train Classifier response ["+train_classifier(classifier)+"]"

def flush_training(states):
  for state in states:
    action = "DREDELETEDOC?StateID="+state+"&IgnoreMaxPendingItems=True"
    autnresponse = index.action(action)
    print(autnresponse)

#main
#load_content(idx)
for classifier in classifiers:
  for ff in ff_scenarios:
    print "creating classifier ["+classifier+"] wth ff ["+ff+"] response ["+\
      classifier_create(classifier,ff)+"]"
    create_classes(classifier,ff)
    print "Train Classifier response ["+train_classifier(classifier)+"]"
    results = dict()
    summary = dict()
    overview = \
      {'true': {'positive': 0, 'negative': 0}, 
      'false': {'positive': 0, 'negative': 0}}
    init_results(classifier)
    traverse_docs(classifier)
    present_results(classifier,ff)
outFile.close()
